filename = input('Filename: ')
texture = input('Texture: ')
output_filename = filename[0:-4] + '.map'
raw_contents = []
final_contents = ''
open_file = open(filename, 'r')
for line in open_file:
    raw_contents.append(line.strip())
open_file.close()

temp_list = []
coordinates_list = []
destination_coordinates_list = []
index = 0
destX = 0 
destY = 0

for line in raw_contents:
    line.strip()
    temp_list = line.split(',')
    for gid in temp_list:
        if gid.isnumeric():
            if int(gid) > 26:
                y = int(int(gid) / 27)
                x = (int(gid) - (y * 27)) - 1
            else:
                y = 0
                x = int(gid) - 1
            coordinates_list.append(str(x) + ', ' + str(y))
    temp_list.clear()

for numY in range(50):
    for numX in range(50):
        destination_coordinates_list.append(str(numX) + ', ' \
                                            + str(numY))
for num in range(50 * 50):
    if coordinates_list[num].find('-1') == -1:
        final_contents += 'graphics->drawCropped(' + texture + ', ' \
                          + str(coordinates_list[num]) \
                          + ', ' + '1, 1, ' \
                          + destination_coordinates_list[num] + ');\n'
open_file = open(output_filename, 'w')
open_file.write(final_contents)
open_file.close()
