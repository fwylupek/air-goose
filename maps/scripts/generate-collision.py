filename = input('Filename: ')
output_filename = filename[0:-4] + '.collision'
raw_contents = []
final_contents = ''
open_file = open(filename, 'r')
for line in open_file:
    raw_contents.append(line.strip())
open_file.close()

temp_list = []
coordinates_list = []
destination_coordinates_list = []
collision = False

for line in raw_contents:
    line.strip()
    temp_list = line.split(',')
    for gid in temp_list:
        if gid.isnumeric():
            if int(gid) != 0:
                collision = True
            coordinates_list.append(collision)
            collision = False
    temp_list.clear()

for numY in range(50):
    for numX in range(50):
        destination_coordinates_list.append(str(numX) + ', ' \
                                            + str(numY))
for num in range(50 * 50):
    if coordinates_list[num]:
        final_contents += 'collision->box(' \
                          + destination_coordinates_list[num] \
                          + ', 64, 64);\n'

open_file = open(output_filename, 'w')
open_file.write(final_contents)
open_file.close()
