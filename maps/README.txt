# How to Auto-Generate a Tiled Map

You can use Tiled (https://www.mapeditor.org/) to create a tiled map with the
*.tsx extension. Remove all lines in the file except for the coordinates inside
the tags for one layer. From there, you can run generate-map.py in the scripts/
directory to auto-generate the code (with a *.map extension) for the engine to
render the that layer of the map, or run generate-collision.py to auto-generate
the code (with a *.collision extension) for the engine to create collision blocks
to prevent movement in desired areas. The resulting code can then be copied into
the *.cpp file(s) prior to compiling the executable for the game.

Note: The *.tsx file must be in the same directory as either script.
