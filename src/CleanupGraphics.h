// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.
//
// The function cleanup() determines the type (SDL_Window, SDL_Renderer,
// SDL_Texture, or SDL_Surface) and destroys it.

#ifndef CleanupGraphics_H
#define CleanupGraphics_H

#include <SDL2/SDL.h>

// Goes through list of arguments to clean up.
template<typename T, typename... Args>
void cleanup(T *t, Args&&... args)
{
    //Clean up the first item.
    cleanup(t);
    //Clean up the remaining arguments.
    cleanup(std::forward<Args>(args)...);
}

// Templates for the items cleanup() can handle.
template<>
void cleanup<SDL_Window>(SDL_Window *win)
{
    if (!win)
    {
    return;
    }
    SDL_DestroyWindow(win);
}
template<>
void cleanup<SDL_Renderer>(SDL_Renderer *ren)
{
    if (!ren)
    {
    return;
    }
    SDL_DestroyRenderer(ren);
}
template<>
void cleanup<SDL_Texture>(SDL_Texture *tex)
{
    if (!tex)
    {
    return;
    }
    SDL_DestroyTexture(tex);
}
template<>
void cleanup<SDL_Surface>(SDL_Surface *surf)
{
    if (!surf)
    {
    return;
    }
    SDL_FreeSurface(surf);
}

#endif
