// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.

#ifndef Player_H
#define Player_H

#include "Graphics.h"
#include "Timer.h"
#include "Collision.h"

class Player
{
    public:
        Player(int startX, int startY,
               char startDirection,
               Graphics *pGraphics);
        ~Player();
        bool interaction(Uint16 x, Uint16 y, char direction);
        void draw(Graphics *graphics);
        int pSpeed;
        bool moveLeft;
        bool moveRight;
        bool moveUp;
        bool moveDown;
        bool handleInput(SDL_Event& event);
        bool messageInput();
        void update(Collision *collision);
        SDL_Rect getRect();
        SDL_Rect getCollisionRect();
        bool useKey;
        bool prompt;

    private:
        Timer *pTime;
        SDL_Rect pRect;
        SDL_Rect pCollisionRect;
        SDL_Rect pImageSrc;
        SDL_Texture *pImage;
        bool flipImage;
        double degrees;
        bool isMoving;
        char direction;
        int cBoxID;
};

#endif
