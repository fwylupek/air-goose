// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.

#ifndef Timer_H
#define Timer_H

#include <stdlib.h>
#include <iostream>
#include <string>
#include <sstream>
#include <SDL2/SDL.h>

using namespace std;

class Timer
{
    public:
        Timer();
        ~Timer();
        float timeSinceCreation();
        float timeSinceLastFrame();
        void regulateFramerate(const int MAX_FRAMERATE);
        bool animationTime();
        void resetAnimationTime();
        bool callRandSecond();
        void resetRandSecond();
        bool callMillisecond();
        bool callSecond();
    
    private:
        float m_timeOfLastCall;
        int currentTime;
        unsigned int lastTime;
        int lastRandSecond;
        unsigned int lastMillisecond;
        unsigned int lastSecond;
        const char *frames;
};

#endif
