// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.
//
// This class provides methods for the user to interact with the game, including
// what their avatar looks like, and how it moves and its actions.

#include "Player.h"

// @param startX Absolute x coordinate in units of 16 pixels.
// @param startY Absolute y coordinate in units of 16 pixels.
// @param startDirection Starting direction; (l)eft, (r)ight, (u)p, (d)own.
// @param pGraphics Pointer to the Graphics object to access the backbuffer.
Player::Player(int startX, int startY,
               char startDirection,
               Graphics *pGraphics)
{
    pRect.x = startX * 64;
    pRect.y = startY * 64;
    pRect.w = 64;
    pRect.h = 128;
    pImageSrc.w = 16;
    pImageSrc.h = 32;
    direction = startDirection;
    pImage = pGraphics->loadTexture("../rsc/sprites/bob.png");
    pTime = new Timer();
    degrees = 0;
    pSpeed = 8;
    moveLeft = false;
    moveRight = false;
    moveUp = false;
    moveDown = false;
    isMoving = false;
    useKey = false;
    prompt = false;
    cBoxID = 511;
    flipImage = false;

    switch (direction)
    {
        case 'l':
            pImageSrc.y = 32;
            break;

        case 'r':
            pImageSrc.y = 96;
            break;

        case 'u':
            pImageSrc.y = 0;
            break;

        case 'd':
            pImageSrc.y = 64;
            break;
    }

    pImageSrc.x = 0;
}

Player::~Player()
{
    SDL_DestroyTexture(pImage);
}

// Handles input events from the human.
// @param event Address of the SDL_Event.
bool Player::handleInput(SDL_Event& event)
{

    while (SDL_PollEvent(&event))
    {
        if (event.type == SDL_QUIT)
        {
            return false;
        }
        if (event.type == SDL_KEYDOWN)
        {
            if (isMoving)
            {
                moveUp = false;
                moveDown = false;
                moveLeft = false;
                moveRight = false;
            }
            if (event.key.keysym.sym == SDLK_w)
            {
                moveUp = true;
                isMoving = true;
            }
            if (event.key.keysym.sym == SDLK_a)
            {
                moveLeft = true;
                isMoving = true;
            }
            if (event.key.keysym.sym == SDLK_s)
            {
                moveDown = true;
                isMoving = true;
            }
            if (event.key.keysym.sym == SDLK_d)
            {
                moveRight = true;
                isMoving = true;
            }
            if (event.key.keysym.sym == SDLK_RETURN)
            {
                useKey = true;
                getCollisionRect();
            }
            if (event.key.keysym.sym == SDLK_ESCAPE)
            {
                return false;
            }
        }

        if (event.type == SDL_KEYUP && isMoving)
        {
            if (event.key.keysym.sym == SDLK_w)
            {
                moveUp = false;
            }
            if (event.key.keysym.sym == SDLK_a)
            {
                moveLeft = false;
            }
            if (event.key.keysym.sym == SDLK_s)
            {
                moveDown = false;
            }
            if (event.key.keysym.sym == SDLK_d)
            {
                moveRight = false;
            }
            if (event.key.keysym.sym == SDLK_RETURN)
            {
                useKey = false;
            }
            if (event.key.keysym.sym == SDLK_ESCAPE)
            {
                return false;
            }
        }
    }

    return true;
}

// How the human interacts with the world.
// @param x The absolute x coordinate in units of 64 pixels.
// @param y The absolute y coordinate in units of 64 pixels.
// @return True if the use key is held down, and coordinates match.
bool Player::interaction(Uint16 x, Uint16 y, char direction)
{
    Uint8 requestedDirection = 0;

    switch (direction)
    {
        case 'l':
            requestedDirection = 32;
            break;

        case 'r':
            requestedDirection = 96;
            break;

        case 'u':
            requestedDirection = 0;
            break;

        case 'd':
            requestedDirection = 64;
            break;
        case 'a':
            requestedDirection = 13;
            break;
    }
    x = x * 64;
    y = y * 64;
    getCollisionRect();

    if (pImageSrc.y == requestedDirection || requestedDirection == 13)
    {
        if (pCollisionRect.x >= x - 64 && pCollisionRect.y >= y - 38 &&
            pCollisionRect.x <= x + 64 && pCollisionRect.y <= y + 72 &&
            useKey == true && pTime->callSecond())
        {
            useKey = false;
            isMoving = false;

            return true;
        }
    }

    return false;
}

// Adds the player's image to the backbuffer.
// @param graphics Pointer to the Graphics object.
void Player::draw(Graphics *graphics)
{
    SDL_Rect camera = graphics->getCameraRect();
    SDL_Rect dstRect = pRect;
    dstRect.x = pRect.x - camera.x;
    dstRect.y = pRect.y - camera.y;

    if (flipImage == true)
    {
        SDL_RenderCopyEx(graphics->getRenderer(), pImage,
                         &pImageSrc, &dstRect, degrees, NULL,
                         SDL_FLIP_HORIZONTAL);
    }
    else
    {
        SDL_RenderCopyEx(graphics->getRenderer(), pImage,
                         &pImageSrc, &dstRect, degrees, NULL,
                         SDL_FLIP_NONE);
    }
}

// Checks for collision, and updates the player's position in the world.
// @param collision Pointer to the Collision object.
void Player::update(Collision *collision)
{
    useKey = false;
    if (moveRight == false &&
        moveUp == false &&
        moveDown == false &&
        moveLeft == false)
    {
        isMoving = false;
        pImageSrc.x = 0;
    }

    if (prompt)
    {
        isMoving = false;
    }

    if (!isMoving)
    {
        moveRight = false;
        moveUp = false;
        moveDown = false;
        moveLeft = false;
    }

    if (moveLeft)
    {
        if (pTime->callMillisecond())
        {
            pRect.x -= pSpeed;
            if (collision->entity(getCollisionRect(), cBoxID))
            {
                pRect.x += pSpeed;
            }
        }
        moveRight = false;
        moveUp = false;
        moveDown = false;
        pImageSrc.y = 32;
        if (pTime->animationTime())
        {
            if (pImageSrc.x < 48)
            {
                pImageSrc.x += 16;
            }
            else
            {
                pImageSrc.x = 0;
            }
        }
    }
    if (moveRight)
    {
        if (pTime->callMillisecond())
        {
            pRect.x += pSpeed;
            if (collision->entity(getCollisionRect(), cBoxID))
            {
                pRect.x -= pSpeed;
            }
        }
        moveLeft = false;
        moveUp = false;
        moveDown = false;
        flipImage = false;
        pImageSrc.y = 96;
        if (pTime->animationTime())
        {
            if (pImageSrc.x < 48)
            {
                pImageSrc.x += 16;
            }
            else
            {
                pImageSrc.x = 0;
            }
        }
    }
    if (moveUp)
    {
        if (pTime->callMillisecond())
        {
            pRect.y -= pSpeed;
            if (collision->entity(getCollisionRect(), cBoxID))
            {
                pRect.y += pSpeed;
            }
        }
        moveRight = false;
        moveLeft = false;
        moveDown = false;
        pImageSrc.y = 0;
        if (pTime->animationTime())
        {
            if (pImageSrc.x < 48)
            {
                pImageSrc.x += 16;
            }
            else
            {
                pImageSrc.x = 0;
            }
        }
    }
    if (moveDown)
    {
        if (pTime->callMillisecond())
        {
            pRect.y += pSpeed;
            if (collision->entity(getCollisionRect(), cBoxID))
            {
                pRect.y -= pSpeed;
            }
        }
        moveRight = false;
        moveUp = false;
        moveLeft = false;
        pImageSrc.y = 64;
        if (pTime->animationTime())
        {
            if (pImageSrc.x < 48)
            {
                pImageSrc.x += 16;
            }
            else
            {
                pImageSrc.x = 0;
            }
        }
    }
}

// @returns The SDL_Rect of the Player.
SDL_Rect Player::getRect()
{
    return pRect;
}

// @returns The SDL_Rect of the Player's collision.
SDL_Rect Player::getCollisionRect()
{
    pCollisionRect.x = pRect.x;
    pCollisionRect.y = pRect.y + 96;
    pCollisionRect.h = 28;
    pCollisionRect.w = pRect.w;

    return pCollisionRect;
}
