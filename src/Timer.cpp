// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.
//
// This class keeps time for each object created with it.

#include "Timer.h"

Timer::Timer() : m_timeOfLastCall(0.0f), lastTime(0)
{
    SDL_Init(SDL_INIT_TIMER);
}

Timer::~Timer()
{
}

// @return The number of milliseconds since engine initialized.
float Timer::timeSinceCreation()
{
    return SDL_GetTicks();
}

// @return The number of milliseconds since the backbuffer was presented.
float Timer::timeSinceLastFrame()
{
    float thisTime = timeSinceCreation();
    float deltaTime = thisTime - m_timeOfLastCall;
    m_timeOfLastCall = thisTime;

    return deltaTime;
}

// Prevents more frames rendering per second than MAX_FPS.
// @param MAX_FPS Maximum frames per second.
void Timer::regulateFramerate(const int MAX_FPS)
{
    if (1000 / MAX_FPS > SDL_GetTicks() - timeSinceCreation())
        SDL_Delay(1000 / MAX_FPS - (SDL_GetTicks() - timeSinceCreation()));
}

// A timer for cropping sprites to create animations.
// @return True if it is time for the next animation frame.
bool Timer::animationTime()
{
    currentTime = SDL_GetTicks();

    if (currentTime - lastTime > 120)
    {
        lastTime = currentTime;
        return true;
    }
    else
    {
        return false;
    }
}

// Resets timer for animation cycle.
void Timer::resetAnimationTime()
{
    lastTime = 120;
}

// Gives a random time between 1 and 2 seconds.
// @return True if between 1 and 2 seconds has passed since last call.
bool Timer::callRandSecond()
{
     currentTime = SDL_GetTicks();

    if (currentTime - lastRandSecond > 1000 + (rand() % 1000))
    {
        lastRandSecond = currentTime;
        return true;
    }
    else
    {
        return false;
    }
}

// Resets callRandSecond().
void Timer::resetRandSecond()
{
    lastRandSecond = 0;
}

// @return True if 1 millisecond has passed since last call.
bool Timer::callMillisecond()
{
    currentTime = SDL_GetTicks();

    if (currentTime - lastMillisecond > 1)
    {
        lastMillisecond = currentTime;
        return true;
    }
    else
    {
        return false;
    }
}

// @return True if 1 second has passed since last call.
bool Timer::callSecond()
{
    currentTime = SDL_GetTicks();

    if (currentTime - lastSecond > 1000)
    {
        lastSecond = currentTime;
        return true;
    }
    else
    {
        return false;
    }
}
