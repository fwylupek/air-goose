// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.

#ifndef Graphics_H
#define Graphics_H

#include <iostream>
#include <string>
#include <cmath>
#include <stdint.h>
#include <stdlib.h>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "Timer.h"

using namespace std;

class Graphics
{
    public:
        Graphics();
        ~Graphics();
        void updateLevelSize(int w, int h);
        SDL_Renderer *getRenderer();
        SDL_Window *getWindow();
        SDL_Rect getCameraRect();
        void clearScreen();
        SDL_Texture *loadTexture(string path);
        void drawSprite(SDL_Texture *texture, int x, int y);
        void drawCropped(SDL_Texture *texture, int srcX, int srcY, int x, int y);
        void drawCropped(SDL_Texture *texture, int srcX, int srcY,
                         int w, int h, int x, int y);
        void setCamera(SDL_Rect playerRect);
        bool checkBounds(SDL_Rect rect);
        void draw();
        void drawMessageBackground();
        void textBox(string message, int x, int y, int w, int h);
        void messageBox(string text);
        void drawSquare(SDL_Texture *texture, int srcX, int srcY,
                        int x, int y, int w, int h);

    private:
        bool initialize();
        SDL_Window *gWindow;
        SDL_Renderer *gRenderer;
        bool loadSceneOne();
        void titleScreen();
        int SCREEN_WIDTH;
        int SCREEN_HEIGHT;
        const char *bGroundFilename;
        Timer *timer;
        SDL_Rect bGroundRect;
        SDL_Texture *mBackground;
        SDL_Color cWhite;
        TTF_Font *font;
        SDL_Rect camera;
        SDL_Rect levelSize;
};

#endif
