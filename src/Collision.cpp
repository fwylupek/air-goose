// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.
//
// This class controls movement for entities. To create a collision box, call:
// box(x, y, w, h). Use resetBoxes() to reset all boxes.

#include "Collision.h"

using namespace std;

// @param cBoxNumber ID of the collision box.
Collision::Collision()
{
    cBoxNumber = 0;

    // Initialize empty collision boxes.
    // @param cBox[512] Array of collision boxes.
    for (int i = 0; i < 512; i++)
    {
        cBox[i].x = -32;
        cBox[i].y = -32;
        cBox[i].h = 0;
        cBox[i].w = 0;
    }
}

Collision::~Collision()
{
}

// Check all sides of two SDL_Rect's for overlap.
// @param cBox1 An SDL_Rect to check for collision.
// @param cBox2 Another SDL_Rect to check for collision.
// @return True if there is collision between the two arguments.
bool Collision::check(SDL_Rect cBox1, SDL_Rect cBox2)
{
    int cBox1_left, cBox2_left,
        cBox1_right, cBox2_right,
        cBox1_top, cBox2_top,
        cBox1_bottom, cBox2_bottom;

    // Calculate sides of first SDL_Rect.
    cBox1_left = cBox1.x;
    cBox1_right = cBox1.x + cBox1.w;
    cBox1_top = cBox1.y;
    cBox1_bottom = cBox1.y + cBox1.h;

    // Calculate sides of second SDL_Rect.
    cBox2_left = cBox2.x;
    cBox2_right = cBox2.x + cBox2.w;
    cBox2_top = cBox2.y;
    cBox2_bottom = cBox2.y + cBox2.h;

    // Check all sides of both SDL_Rect's.
    if (cBox1_bottom <= cBox2_top)
    {
        return false;
    }
    if (cBox1_top >= cBox2_bottom)
    {
        return false;
    }
    if (cBox1_right <= cBox2_left)
    {
        return false;
    }
    if (cBox1_left >= cBox2_right)
    {
        return false;
    }

    return true;
}

// Checks collision between an SDL_Rect and a cBox[512].
// @param rect An SDL_Rect.
// @param cBoxID The cBoxNumber or override collision.
bool Collision::entity(SDL_Rect rect, int cBoxID)
{
    for(int i = 0; i < 512; i++)
    {
        if (i != cBoxID)
        {
            if (check(rect, cBox[i]))
            {
                return true;
            }
        }
    }

    return false;
}

// Create a collision box with an automatically iterated ID number.
// @param x The x coordinate in units of 64 pixels.
// @param y The y coordinate in units of 64 pixels.
// @param w The width in absolute pixels.
// @param h The height in absolute pixels.
void Collision::box(int x, int y, int w, int h)
{
    cBox[cBoxNumber].x = x * 64;
    cBox[cBoxNumber].y = y * 64;
    cBox[cBoxNumber].w = w;
    cBox[cBoxNumber].h = h;

    cBoxNumber++;
}

// Create a collision box with a specific ID number. Be careful not to
// overwrite with box(x, y, w, h).
// @param x The x coordinate in units of 64 pixels.
// @param y The y coordinate in units of 64 pixels.
// @param w The width in absolute pixels.
// @param h The height in absolute pixels.
// @param boxNumber Manually assigned ID number.
void Collision::box(int x, int y, int w, int h, int boxNumber)
{
    cBox[boxNumber].x = x * 64;
    cBox[boxNumber].y = y * 64;
    cBox[boxNumber].w = w;
    cBox[boxNumber].h = h;
}

// Creates human-sized collision box with ID of cBoxID.
// @param rect An SDL_Rect that contains x and y coordinates of target.
// @param cBoxID The ID number for the collision box.
void Collision::humanBox(SDL_Rect rect, int cBoxID)
{
    cBox[cBoxID].x = rect.x;
    cBox[cBoxID].y = rect.y;
    cBox[cBoxID].w = 56;
    cBox[cBoxID].h = 18;
}

// Sets the collision box for the player.
// @param rect The SDL_Rect of the player.
void Collision::playerBox(SDL_Rect rect)
{
    cBox[511].x = rect.x;
    cBox[511].y = rect.y;
    cBox[511].w = 56;
    cBox[511].h = 18;
}

// Reset all collision boxes and start counting new ID's from 0.
void Collision::resetBoxes()
{
    cBoxNumber = 0;

    for (int i = 0; i < 512; i++)
    {
        cBox[i].x = 0;
        cBox[i].y = 0;
        cBox[i].h = 0;
        cBox[i].w = 0;
    }
}
