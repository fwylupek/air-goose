// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.
//
// This class handles non-playable characters.

#include "NPC.h"

// @param startX Absolute x coordinate in pixels.
// @param startY Absolute y coordinate in pixels.
// @param startDirection Starting direction; (l)eft, (r)ight, (u)p, (d)own.
// @param pGraphics Pointer to the Graphics object to access the backbuffer.
NPC::NPC(int startX, int startY,
         char startDirection,
         Graphics *graphics)
{
    nRect.x = startX;
    nRect.y = startY;
    nRect.w = 64;
    nRect.h = 128;
    imageSrc.h = 128;
    imageSrc.w = 64;
    direction = startDirection;
    image = graphics->loadTexture("../rsc/sprites/base.png");
    nTime = new Timer();
    degrees = 0;
    speed = 1;
    moveLeft = false;
    moveRight = false;
    moveUp = false;
    moveDown = false;
    isMoving = false;
    flipImage = false;

    switch (direction)
    {
        case 'l':
            flipImage = true;
            imageSrc.y = 128;
            break;
        case 'r':
            imageSrc.y = 128;
            break;
        case 'u':
            imageSrc.y = 0;
            break;
        case 'd':
            imageSrc.y = 0;
            break;
    }

    imageSrc.x = 0;
}

NPC::~NPC()
{
    SDL_DestroyTexture(image);
}

// Move the NPC randomly and check for collision.
// @param collision Pointer to the Collision object.
// @param cBoxID Collision box ID number of the NPC.
void NPC::moveRandomly(Collision *collision, int cBoxID)
{
    if (nTime->callRandSecond() && !collision->entity(nRect, cBoxID))
    {
        srand(SDL_GetTicks());
        switch (rand() % 50)
        {
            case 0:
                moveUp = true;
                moveRight = false;
                moveLeft = false;
                moveDown = false;
                flipImage = false;
                break;
            case 1:
                moveDown = true;
                moveRight = false;
                moveUp = false;
                moveLeft = false;
                flipImage = false;
                break;
            case 2:
                moveLeft = true;
                moveRight = false;
                moveUp = false;
                moveDown = false;
                flipImage = true;
                break;
            case 3:
                moveRight = true;
                moveLeft = false;
                moveUp = false;
                moveDown = false;
                flipImage = false;
                break;
            default:
                if (collision->entity(nRect, cBoxID))
                {
                    moveUp = false;
                    moveDown = false;
                    moveLeft = false;
                    moveRight = false;
                    imageSrc.x = 0;
                }
                break;
        }
    }

    if (collision->entity(nRect, cBoxID))
    {
        moveUp = false;
        moveDown = false;
        moveLeft = false;
        moveRight = false;
        imageSrc.x = 0;
    }

    if (moveLeft)
    {
        if (nTime->callMillisecond() &&
            !collision->entity(nRect, cBoxID))
        {
            nRect.x -= speed;
            if (collision->entity(nRect, cBoxID))
            {
                nRect.x += speed;
            }
        }

        imageSrc.y = 128;
        if (nTime->animationTime()
            && !collision->entity(nRect, cBoxID))
        {
            if (imageSrc.x < 256)
            {
                imageSrc.x += 64;
            }
            else
            {
                imageSrc.x = 64;
            }
        }
    }
    if (moveRight)
    {
        if (nTime->callMillisecond() &&
            !collision->entity(nRect, cBoxID))
        {
            nRect.x += speed;
            if (collision->entity(nRect, cBoxID))
            {
                nRect.x -= speed;
            }
        }

        imageSrc.y = 128;
        if (nTime->animationTime()
            && !collision->entity(nRect, cBoxID))
        {
            if (imageSrc.x < 256)
            {
                imageSrc.x += 64;
            }
            else
            {
                imageSrc.x = 64;
            }
        }
    }
    if (moveUp)
    {
        if (nTime->callMillisecond() &&
            !collision->entity(nRect, cBoxID))
        {
            nRect.y -= speed;
            if (collision->entity(nRect, cBoxID))
            {
                nRect.y += speed;
            }
        }

        imageSrc.y = 0;
        if (nTime->animationTime()
            && !collision->entity(nRect, cBoxID))
        {
            if (imageSrc.x < 256)
            {
                imageSrc.x += 64;
            }
            else
            {
                imageSrc.x = 64;
            }
        }
    }
    if (moveDown)
    {
        if (nTime->callMillisecond() &&
            !collision->entity(nRect, cBoxID))
        {
            nRect.y += speed;
            if (collision->entity(nRect, cBoxID))
            {
                nRect.y -= speed;
            }
        }

        imageSrc.y = 0;
        if (nTime->animationTime()
            && !collision->entity(nRect, cBoxID))
        {
            if (imageSrc.x < 256)
            {
                imageSrc.x += 64;
            }
            else
            {
                imageSrc.x = 64;
            }
        }
    }
}

// Copies the image of the NPC to the backbuffer for rendering.
// @param graphics A pointer to the Graphics object.
void NPC::draw(Graphics *graphics)
{
    SDL_Rect camera = graphics->getCameraRect();
    SDL_Rect dstRect = nRect;
    dstRect.x = nRect.x - camera.x;
    dstRect.y = nRect.y - camera.y;

    if (flipImage == true)
    {
        SDL_RenderCopyEx(graphics->getRenderer(), image,
                         &imageSrc, &dstRect, degrees, NULL,
                         SDL_FLIP_HORIZONTAL);
    }
    else
    {

        SDL_RenderCopyEx(graphics->getRenderer(), image,
                         &imageSrc, &dstRect, degrees, NULL,
                         SDL_FLIP_NONE);
    }
}

// @return The SDL_Rect of the NPC object.
SDL_Rect NPC::getRect()
{
    return nRect;
}
