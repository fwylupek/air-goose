// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.

#ifndef NPC_H
#define NPC_H

#include <time.h>
#include "Graphics.h"
#include "Timer.h"
#include "Collision.h"

class NPC
{
    public:
        NPC(int startX, int startY,
            char startDirection,
            Graphics *graphics);
        ~NPC();
        bool interaction(Uint16 x, Uint16 y);
        void draw(Graphics *graphics);
        int speed;
        bool moveLeft;
        bool moveRight;
        bool moveUp;
        bool moveDown;
        void moveRandomly(Collision *collision, int cBoxID);
        SDL_Rect getRect();

    private:
        Timer *nTime;
        SDL_Rect nRect;
        SDL_Rect imageSrc;
        SDL_Texture *image;
        bool flipImage;
        double degrees;
        bool isMoving;
        char direction;
        bool useKey;
};

#endif
