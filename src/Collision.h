// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.

#ifndef Collision_H
#define Collision_H

#include <iostream>
#include <string>
#include <cmath>
#include <stdint.h>
#include <stdlib.h>
#include <algorithm>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include "Timer.h"

using namespace std;

class Collision
{
    public:
        Collision();
        ~Collision();
        bool check(SDL_Rect cBox1, SDL_Rect cBox2);
        bool entity(SDL_Rect playerRect, int cBoxID);
        void box(int x, int y, int w, int h);
        void box(int x, int y, int w, int h, int boxNumber);
        void humanBox(SDL_Rect rect, int cBoxID);
        void playerBox(SDL_Rect rect);
        void resetBoxes();
    
    private:
        unsigned int cBoxNumber;
        SDL_Rect cBox[512];
        SDL_Rect camera;
};

#endif
