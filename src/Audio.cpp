// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.
//
// This class covers anything to do with music and sounds.

#include "Audio.h"

Audio::Audio()
{
    if (initialize())
    {
        timer = new Timer();
    }
}

Audio::~Audio()
{
    delete timer;
    Mix_CloseAudio();
}

// Initialize SDL_mixer API and open audio channels.
bool Audio::initialize()
{
    if (Mix_OpenAudio(22050, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
    {
        return false;
    }
    else
    {
        Mix_AllocateChannels(14);
        return true;
    }
}

// Loads an audio file to use as a sample. Supports WAVE, AIFF, RIFF, OGG, and
// VOC. Use cleanup() to unload.
// @param path The path of the audio file to load.
Mix_Chunk *Audio::loadSound(const char *path)
{
    Mix_Chunk *sound = Mix_LoadWAV(path);

    if (sound == NULL)
    {
        cout << "Unable to load sound from " << path << endl
             << "SDL error: " << SDL_GetError() << endl;
        return NULL;
    }
    else
    {
        return sound;
    }
}

// Unloads an audio sample.
// @param sound A loaded sound sample.
void Audio::cleanup(Mix_Chunk *sound)
{
    Mix_FreeChunk(sound);
}

// Plays a loaded audio sample one time on the next available channel.
// @param sound A loaded sound sample.
void Audio::playSound(Mix_Chunk *sound)
{
    Mix_PlayChannel(-1, sound, 0);
}

// Plays a loaded audio sample one time on the specified channel.
// @param sound A loaded sound sample.
// @param channel The desired channel to play the sound. -1 for next available.
void Audio::playSound(Mix_Chunk *sound, int channel)
{
    Mix_PlayChannel(channel, sound, 0);
}

// Pauses all sound. Use resumeSound() to resume.
void Audio::pauseSound()
{
    Mix_Pause(-1);
}

// Pauses a channel, or all sound. Use resumeSound() to resume.
// @param channel The desired channel to play the sound. -1 for next available.
void Audio::pauseSound(int channel)
{
    Mix_Pause(channel);
}

// Unpauses all sound.
void Audio::resumeSound()
{
    Mix_Resume(-1);
}

// Unpauses a channel, or all sound.
// @param channel The desired channel to play the sound. -1 for next available.
void Audio::resumeSound(int channel)
{
    Mix_Resume(channel);
}

// Stops all sound.
void Audio::stopSound()
{
    Mix_HaltChannel(-1);
}

// Stops a channel, or all sound.
// @param channel The desired channel to play the sound. -1 for next available.
void Audio::stopSound(int channel)
{
    Mix_HaltChannel(channel);
}

// Plays a loaded audio sample on a specific channel for playing music an
// unlimited number of times. Use pauseMusic() or stopMusic() to stop. The
// more general methods pauseSound() and stopSound() also work.
// @param sound A loaded sound sample.
void Audio::playMusic(Mix_Chunk *music)
{
    Mix_PlayChannel(13, music, -1);
}

// Pauses music channel.
void Audio::pauseMusic()
{
    Mix_Pause(13);
}

// Unpauses music.
void Audio::resumeMusic()
{
    Mix_Resume(13);
}

// Stops music channel.
void Audio::stopMusic()
{
    Mix_HaltChannel(13);
}
