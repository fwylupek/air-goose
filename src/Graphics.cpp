// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.
//
// This class covers anything to do with visible graphics, including loading,
// drawing, and manipulating textures, text, and visual effects. The "camera,"
// or presently visible area, is defined by the detected resolution. Classes
// that depend on Graphics are Player and NPC. Use loadTexture() to place an
// image into memory, and use cleanup() to remove it.

#include "Graphics.h"
#include "CleanupGraphics.h"

Graphics::Graphics()
{
    if (initialize())
    {
        timer = new Timer();
    }
    titleScreen();
}

Graphics::~Graphics()
{
    TTF_CloseFont(font);
    cleanup(gRenderer, gWindow);

    IMG_Quit();
    SDL_Quit();
}

// Initialize subsystems, detect resolution, create window and renderer,
// define environment, load message background and font, and set camera.
// @return True if successful.
bool Graphics::initialize()
{
    // Initialize SDL subsystems.
    if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
    {
        cout << "SDL could not initialize. SDL Error: "
             << SDL_GetError() << endl;

        return false;
    }
    else
    {
        // Detect resolution.
        SDL_DisplayMode dispMode;
        SDL_GetDesktopDisplayMode(0, &dispMode);
        SCREEN_WIDTH = dispMode.w;
        SCREEN_HEIGHT = dispMode.h;

        // Create window.
        gWindow = SDL_CreateWindow("Air Goose Game Engine",
                                   SDL_WINDOWPOS_UNDEFINED,
                                   SDL_WINDOWPOS_UNDEFINED,
                                   SCREEN_WIDTH, SCREEN_HEIGHT,
                                   SDL_WINDOW_FULLSCREEN |
                                   SDL_WINDOW_OPENGL);
        
        // Hide mouse cursor.
        //SDL_ShowCursor(SDL_DISABLE);
        
        if (gWindow == NULL)
        {
            cout << "Window could not be created." << endl
                 << "SDL Error: " << SDL_GetError() << endl;
            return false;
        }
        else
        {
            // Create renderer.
            gRenderer = SDL_CreateRenderer(gWindow, -1,
                                           SDL_RENDERER_ACCELERATED);
            if(gRenderer == NULL)
            {
                cout << "Renderer could not be created." << endl
                     << "SDL Error: " << SDL_GetError() << endl;
                return false;
            }
            else
            {
                // Set color for drawing shapes to white.
                SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 0);
                int imgFlags = IMG_INIT_PNG;
                
                if (!(IMG_Init(imgFlags) & imgFlags))
                {
                    cout << "SDL_image could not initialize." << endl
                         << "SDL_image Error: " << IMG_GetError() << endl;
                    return false;
                }
            }
        }
        
        // Set font color to white.
        cWhite = {255, 255, 255, 255};
        mBackground = loadTexture("../rsc/gfx/message-background.png");
        if (TTF_Init() == -1)
        {
            cout << "Failed to initialize true type font." << endl
                 << "SDL error: " << SDL_GetError() << endl;
            return false;
        }
        else
        {
            // Open TrueType font.
            font = TTF_OpenFont("../rsc/font/PressStart2P.ttf", 100);
            if (font == NULL)
            {
                cout << "Failed to load font." << endl
                     << "SDL error: " << SDL_GetError() << endl;
                return false;
            }
        }
    }

    camera = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

    return true;
}

// Loads image as surface, then converts to texture. Use cleanup() to unload.
// @param path The path of the image to load.
SDL_Texture *Graphics::loadTexture(string path)
{
    SDL_Texture *newTexture = NULL;
    SDL_Surface *loadedSurface = IMG_Load(path.c_str());

    if (loadedSurface == NULL)
    {
        cout << "Unable to load image from " << path.c_str() << endl
             << "SDL error: " << SDL_GetError() << endl;
        return NULL;
    }
    else
    {
        newTexture = SDL_CreateTextureFromSurface(gRenderer, loadedSurface);
        if(newTexture == NULL)
        {
            cout << "Unable to create texture from " << path.c_str() << endl
                 << "SDL error: " << SDL_GetError() << endl;
            return NULL;
        }

        cleanup(loadedSurface);
    }

    return newTexture;
}

// Changes the size of the level, or playable area.
// @param w Absolute width in units of 16 pixels.
// @param h Absolute height in units of 16 pixels.
void Graphics::updateLevelSize(int w, int h)
{
    levelSize.w = w * 64;
    levelSize.h = h * 64;
}

// Displays a loaded texture.
// @param texture A loaded texture.
// @param x The destination x coordinate in units of 64.
// @param y The destination y coordinate in units of 64.
void Graphics::drawSprite(SDL_Texture *texture, int x, int y)
{
    SDL_Rect dstRect = {x * 64, y * 64, 64, 64};

    if (checkBounds(dstRect))
    {
        dstRect.x = x - camera.x;
        dstRect.y = y - camera.y;
        SDL_RenderCopy(gRenderer, texture, NULL, &dstRect);
    }
}

void Graphics::drawCropped(SDL_Texture *texture, int srcX, int srcY, int x, int y)
{
    SDL_Rect srcRect = {srcX * 16, srcY * 16, 16, 16};
    SDL_Rect dstRect = {x * 64, y * 64, 64, 64};

    if (checkBounds(dstRect))
    {
        dstRect.x = (x * 64) - camera.x;
        dstRect.y = (y * 64) - camera.y;
        SDL_RenderCopy(gRenderer, texture, &srcRect, &dstRect);
    }
}

void Graphics::drawCropped(SDL_Texture *texture, int srcX, int srcY,
                           int w, int h, int x, int y)
{
    SDL_Rect srcRect = {srcX * 16, srcY * 16, w * 16, h * 16};
    SDL_Rect dstRect = {x * 64, y * 64, w * 64, h * 64};

    if (checkBounds(dstRect))
    {
        dstRect.x = (x * 64) - camera.x;
        dstRect.y = (y * 64) - camera.y;
        SDL_RenderCopy(gRenderer, texture, &srcRect, &dstRect);
    }
}

// Draws repeating texture in a square.
// @param texture A loaded texture.
// @param x Destination x coordinate.
// @param y Destination y coordinate.
// @param w The width in units of 16.
// @param h The height in units of 16.
void Graphics::drawSquare(SDL_Texture *texture, int srcX, int srcY,
                          int x, int y, int w, int h)
{
    SDL_Rect srcRect = {srcX, srcY, 16, 16};
    SDL_Rect dstSquare = {x, y, 64, 64};

    for (int x = 0; x < w; x++)
    {
        for (int y = 0; y < h; y++)
        {
            dstSquare.x = x * 64;
            dstSquare.y = y * 64;
            if (checkBounds(dstSquare))
            {
                dstSquare.x -= camera.x;
                dstSquare.y -= camera.y;
                SDL_RenderCopy(gRenderer, texture, &srcRect, &dstSquare);
            }
        }
    }
}

// Clear screen with white color.
void Graphics::clearScreen()
{
    SDL_SetRenderDrawColor(gRenderer,
                           0xFF, 0xFF,
                           0xFF, 0xFF);
    SDL_RenderClear(gRenderer);
}

// Show opening title screen. (Currently this shows a color fade animation.)
void Graphics::titleScreen()
{
    SDL_Event event;
    int r = 153;
    int g = 153;
    int b = 255;
    bool decreasingColor = false;
    bGroundRect = {0, 0, SCREEN_WIDTH, SCREEN_HEIGHT};

    while (true)
    {
        // Wait for input.
        while (SDL_PollEvent(&event) != 0)
        {
            if (event.type == SDL_QUIT)
            {
                return;
            }
            if (event.type == SDL_KEYDOWN)
            {
                return;
            }
        }

        SDL_SetRenderDrawColor(gRenderer,
                               0xFF, 0xFF,
                               0xFF, 0xFF);
        SDL_RenderClear(gRenderer);

        // Fade background color.
        SDL_SetRenderDrawColor(gRenderer, r, g, b, 0);
        if (timer->callMillisecond())
        {
            if (decreasingColor)
            {
                r -= 1;
                g -= 1;
                if (r <= 0)
                {
                    decreasingColor = false;
                }
            }
            else
            {
                r += 1;
                g += 1;
                if (r >= 255)
                {
                    decreasingColor = true;
                }
            }
        }

        SDL_RenderFillRect(gRenderer, &bGroundRect);
        SDL_RenderPresent(gRenderer);
        event = {};
    }
}

SDL_Renderer *Graphics::getRenderer()
{
    return gRenderer;
}

SDL_Window *Graphics::getWindow()
{
    return gWindow;
}

SDL_Rect Graphics::getCameraRect()
{
    return camera;
}

// Update and present the backbuffer to the screen, then clear backbuffer.
void Graphics::draw()
{
    SDL_RenderPresent(gRenderer);
    SDL_RenderClear(gRenderer);
}

// Draws message window background color in a square.
void Graphics::drawMessageBackground()
{
    drawSquare(mBackground, 0, 0, SCREEN_WIDTH * 0.1, 0, 
               (SCREEN_WIDTH / 22) - (SCREEN_WIDTH * 0.1), SCREEN_WIDTH / 22);
}

// Prints the desired message to the screen.
// @param message Desired text to print.
// @param x Destination x coordinate.
// @param y Destination y coordinate.
// @param w Width of text in pixels.
// @param h Height of text in pixels.
void Graphics::textBox(string message, int x, int y, int w, int h)
{
    SDL_Rect textRect;
    textRect.h = h;
    textRect.w = w;
    textRect.x = x;
    textRect.y = y;

    SDL_Surface *tempSurf = TTF_RenderText_Solid(font, message.c_str(), cWhite);
    SDL_Texture *tempTexture = SDL_CreateTextureFromSurface(gRenderer, tempSurf);
    SDL_RenderCopy(gRenderer, tempTexture, NULL, &textRect);

    cleanup(tempSurf, tempTexture);

    SDL_RenderPresent(gRenderer);
}

// Print desired text to screen with fancy blending. Text is cut and
// concatenated to display 9 rows of ~36 characters each.
// @param text Text to display with background. Supports about 99 characters.
void Graphics::messageBox(string text)
{
    SDL_Rect textRect;
    SDL_Rect mRect;

    mRect.x = 0;
    mRect.y = SCREEN_HEIGHT * 0.7;
    mRect.w = SCREEN_WIDTH;
    mRect.h = SCREEN_HEIGHT * 0.31;

    Uint32 clip = 0;
    Uint32 rows = text.length() / 36;
    string str[3];

    switch (rows)
    {
        // In the case of one line, print the text at the top left of the box.
        case 0:
        {
            textRect.x = SCREEN_WIDTH * 0.01;
            textRect.y = mRect.y;
            textRect.y += mRect.y * 0.04;
            textRect.w = (SCREEN_WIDTH / 37) * text.length();
            textRect.h = mRect.h / 4;
            SDL_Surface *tempSurf = TTF_RenderText_Blended(font, text.c_str(),
                                                           cWhite);
            SDL_Texture *tempTexture = SDL_CreateTextureFromSurface(gRenderer,
                                                                    tempSurf);

            // Draw message box background.
            SDL_RenderCopy(gRenderer, mBackground, NULL, &mRect);
            SDL_RenderCopy(gRenderer, tempTexture, NULL, &textRect);
            SDL_RenderPresent(gRenderer);
            cleanup(tempSurf, tempTexture);
        }
        break;

        // Cut the text in two and print on two lines. 
        case 1:
        {
            // Store substrings in two array indexes in str[3].
            str[0] = text.substr(0, 36);
            clip = str[0].find_last_of(" ");
            str[0].erase(clip, 36 - clip);
            str[1] = text.substr(clip + 1, text.length() - clip);
            textRect.x = SCREEN_WIDTH * 0.01;
            textRect.y = mRect.y;
            textRect.y += mRect.y * 0.04;
            textRect.w = (SCREEN_WIDTH / 37) * str[0].length();
            textRect.h = mRect.h / 4;
            SDL_Surface *tempSurf = TTF_RenderText_Blended(font, str[0].c_str(),
                                                           cWhite);
            SDL_Texture *tempTexture = SDL_CreateTextureFromSurface(gRenderer,
                                                                    tempSurf);
            SDL_RenderCopy(gRenderer, mBackground, NULL, &mRect);
            SDL_RenderCopy(gRenderer, tempTexture, NULL, &textRect);
            textRect.y += textRect.h;
            textRect.w = (SCREEN_WIDTH / 37) * str[1].length();
            tempSurf = TTF_RenderText_Blended(font, str[1].c_str(), cWhite);
            tempTexture = SDL_CreateTextureFromSurface(gRenderer, tempSurf);
            SDL_RenderCopy(gRenderer, tempTexture, NULL, &textRect);
            SDL_RenderPresent(gRenderer);
            cleanup(tempSurf, tempTexture);
        }
        break;

        // Do case 1, and then do it again for the resulting second line.
        case 2:
        {
            // Store three substrings in str[3].
            str[0] = text.substr(0, 36);
            clip = str[0].find_last_of(" ");
            str[0].erase(clip, 36 - clip);
            str[1] = text.substr(clip + 1, 36);
            clip = str[1].find_last_of(" ");
            str[1].erase(clip + 1, 36 - clip);
            str[2] = text.substr(str[0].length() + str[1].length() + 1,
                                 text.length() - clip);
            textRect.x = SCREEN_WIDTH * 0.01;
            textRect.y = mRect.y;
            textRect.y += mRect.y * 0.04;
            textRect.w = (SCREEN_WIDTH / 37) * str[0].length();
            textRect.h = mRect.h / 4;
            SDL_Surface *tempSurf = TTF_RenderText_Blended(font, str[0].c_str(), 
                                                           cWhite);
            SDL_Texture *tempTexture = SDL_CreateTextureFromSurface(gRenderer,
                                                                    tempSurf);
            SDL_RenderCopy(gRenderer, mBackground, NULL, &mRect);
            SDL_RenderCopy(gRenderer, tempTexture, NULL, &textRect);
            textRect.y += textRect.h;
            textRect.w = (SCREEN_WIDTH / 37) * str[1].length();
            tempSurf = TTF_RenderText_Blended(font, str[1].c_str(), cWhite);
            tempTexture = SDL_CreateTextureFromSurface(gRenderer, tempSurf);
            SDL_RenderCopy(gRenderer, tempTexture, NULL, &textRect);
            textRect.y += textRect.h;
            textRect.w = (SCREEN_WIDTH / 37) * str[2].length();
            tempSurf = TTF_RenderText_Blended(font, str[2].c_str(), cWhite);
            tempTexture = SDL_CreateTextureFromSurface(gRenderer, tempSurf);
            SDL_RenderCopy(gRenderer, tempTexture, NULL, &textRect);
            SDL_RenderPresent(gRenderer);
            cleanup(tempSurf, tempTexture);
        }
        break;

        // Break if too much text is passed.
        default:
            cout << "Error: String too large:" << endl << text << endl;
            break;
    }

    SDL_RenderPresent(gRenderer);

    // TODO: Return false if return key is still held down from before.
    SDL_Event event = {};
    bool input = false;

    while (!input)
    {
        SDL_Delay(300);
        if (SDL_PollEvent(&event))
        {
            if (event.type == SDL_KEYDOWN)
            {
                if (event.key.keysym.sym == SDLK_RETURN)
                {
                    input = true;
                }
            }

            if (event.type == SDL_KEYUP)
            {
                if (event.key.keysym.sym == SDLK_RETURN)
                {
                    input = false;
                }
            }
        }
    }
}

// Centers the camera over the player.
// @param playerRect SDL_Rect of the Player class.
void Graphics::setCamera(SDL_Rect playerRect)
{
    // Center camera over player.
    camera.x = (playerRect.x - SCREEN_WIDTH / 2) + 64;
    camera.y = (playerRect.y - SCREEN_HEIGHT / 2) + 64;

    // Keep camera in bounds.
    if (camera.x < 0)
    {
        camera.x = 0;
    }
    if (camera.y < 0)
    {
        camera.y = 0;
    }
    if (camera.x > levelSize.w - camera.w)
    {
        camera.x = levelSize.w - camera.w;
    }
    if (camera.y > levelSize.h - camera.h)
    {
        camera.y = levelSize.h - camera.h;
    }
}

// Checks if the object is within camera boundaries.
// @return True if object is within camera boundries.
bool Graphics::checkBounds(SDL_Rect rect)
{
    if (rect.x + rect.w < camera.x)
    {
        return false;
    }
    if (rect.x > camera.x + camera.w)
    {
        return false;
    }
    if (rect.y + rect.h < camera.y)
    {
        return false;
    }
    if (rect.y > camera.y + camera.h)
    {
        return false;
    }

    return true;
}
