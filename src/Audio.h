// Air Goose - A 2D game engine using SDL 2
// Copyright (C) 2019 Frank Wylupek
//
// Air Goose is free software: you can redistribute it and/or modify it under
// the terms of the GNU General Public License as published by the Free 
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// Air Goose is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Air Goose.  If not, see <http://www.gnu.org/licenses/>.

#ifndef Audio_H
#define Audio_H

#include <iostream>
#include <string>
#include <SDL2/SDL_mixer.h>
#include "Timer.h"

using namespace std;

class Audio
{
    public:
        Audio();
        ~Audio();
        Mix_Chunk *loadSound(const char *path);
        void cleanup(Mix_Chunk *sound);
        void playSound(Mix_Chunk *sound);
        void playSound(Mix_Chunk *sound, int channel);
        void pauseSound();
        void pauseSound(int channel);
        void resumeSound();
        void resumeSound(int channel);
        void stopSound();
        void stopSound(int channel);
        void playMusic(Mix_Chunk *music);
        void pauseMusic();
        void resumeMusic();
        void stopMusic();

    private:
        bool initialize();
        Timer *timer;
};

#endif
