# Contributing

## Reporting issues

If you find an issue in the engine, open an issue or email air-goose@tuta.io
and include as much information as necessary, including steps to reproduce
the issue.

## Developing changes and additions

If you can make contributions, follow the
[Allman C++ style](https://en.wikipedia.org/wiki/Indentation_style#Allman_style),
and open a pull request in the `develop` branch (this branch), or email 
air-goose@tuta.io.

## Building

* Install the SDL 2 development libraries.
* Check documentation in the \*.cpp files.
* Use methods from objects in Main.cpp to code your game.
* Run `make` in the air-goose/src directory.
