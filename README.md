# Air Goose - A 2D game engine using SDL 2

Air Goose is a free and open source game engine for 2D games with support for
animations, text, collision, and audio. It is licensed under GNU General 
Public License, version 3 or later (GPLv3+).

## This branch (develop)

This branch is the integration branch. New development changes are delivered
here first. When this branch reaches a stable point, it will be merged into
the `master` branch.

Make sure to aim pull requests in the right direction.

## Getting involved

This project is open source in the hope that it will be useful to people, and
that the open source community will be useful to the project. Read more about
contributing [here](CONTRIBUTING.md).

## Building

This code is written in C++, using [SDL 2](https://www.libsdl.org/) libraries,
and will run on Windows, Mac, or Linux.

To compile, verify that a C++ compiler, make, and SDL 2 libraries are
installed and run `make` in  the air-goose/src/ directory.
